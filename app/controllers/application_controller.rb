class ApplicationController < ActionController::API
    before_action :configure_permitted_parameters, if: :devise_controller?

    protected
  
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    end
    
    include DeviseTokenAuth::Concerns::SetUserByToken
    require "active_model_serializers"
    include Authenticable

    
  
end
