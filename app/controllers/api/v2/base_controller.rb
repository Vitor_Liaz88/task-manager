class Api::V2::BaseController < ApplicationController
    include Authenticable
    include DeviseTokenAuth::Concerns::SetUserByToken
end