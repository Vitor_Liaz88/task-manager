class Api::V1::BaseController < ApplicationController
    include Authenticable
    include DeviseTokenAuth::Concerns::SetUserByToken
end